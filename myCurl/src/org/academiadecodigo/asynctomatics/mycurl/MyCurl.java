package org.academiadecodigo.asynctomatics.mycurl;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class MyCurl {

    public static void main(String[] args) {

        // vamos obter toda pagina do address
        try {
            new MyCurl().getSource("https://www.google.com");

        } catch (MalformedURLException ignored) { // isto significa que estamos a ignorar
            // conscientemente esta exceção
            System.err.println("Incorrect address");
        }
    }

    public void getSource(String address) throws MalformedURLException {

        // erro é culpa do utilizador
        // classe de java
        URL url = new URL(address);

        // precisamos de ter isto fora do try para depois fechar o stream
        InputStream urlStream = null;

        try {
            // vou precisar de uma stream p ler o url
            urlStream = new BufferedInputStream(url.openStream());
            // este reader vai ler
            InputStreamReader reader = new InputStreamReader(urlStream);

            // metodo que vai ler char a char e retorna -1 a dizer q parou de ler
            // loop para continuar a ler
            int charToRead;
            while ((charToRead = reader.read()) != -1) {
                // usar o print sem ln para conseguir ler todos os caracteres seguidos
                // tambem o ln é um caracter
                // o metodo read retorna um inteiro
                // necessitamos do cast para garantir que lemos no sout em characters
                System.out.print((char)charToRead);
            }

        } catch (IOException e) {
            // este metodo consegue seguir os metados e dizer exatamente onde está o erro
            e.printStackTrace();

        } finally {
            cleanUp(urlStream);
        }
    }

    private void cleanUp(Closeable closeable) {

        try {
            // garante que só fecha se nao for null
            // se eu fechar só esta, todos os outros recursos que estao dependo desta tambem sao
            // fechados como o input stream
            if (closeable != null) {
                closeable.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
