package org.academiadecodigo.asynctomatics.tests.money;

public class Money {

    private int value;
    private String currency;

    public Money(int value, String currency) {

        if (value <= 0) {
            throw new IllegalArgumentException("Amount should be higher than 0.");
        }

        if (currency == null || currency.isEmpty()) {
            throw new IllegalArgumentException("Currency should not be empty.");
        }

        this.value = value;
        this.currency = currency;
    }

    public int getValue() {
        return value;
    }

    public String getCurrency() {
        return currency;
    }
}
