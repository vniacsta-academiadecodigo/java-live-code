package org.academiadecodigo.asynctomatics.tests.supercalculator;

// this class is to emulate a request over the network
public interface BrainServer {

    int add(int num1, int num2);
}
