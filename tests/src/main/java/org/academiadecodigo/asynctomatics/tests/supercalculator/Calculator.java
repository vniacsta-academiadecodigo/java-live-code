package org.academiadecodigo.asynctomatics.tests.supercalculator;

public class Calculator {

    private BrainServer brain;
    private DisplayResult display;
    private int battery;

    public Calculator(BrainServer brain, DisplayResult display) {

        this.brain = brain;
        this.display = display;

        battery = 100;
    }

    public void add() {

        if (battery <= 0) {
            display.showError("Battery is dead");
        }

        useBattery();

        /*
        Introducing IO into the mix leaves our tests even more prone to error
        since we can't assure the consistency of the results
         */
        int firstOperand = display.askUserForOperand();
        int secondOperand = display.askUserForOperand();

        display.showResult(brain.add(firstOperand, secondOperand));

    }

    private void useBattery() {

        if (battery <= 10) {
            battery = 0;
            return;
        }

        battery -= 10;
    }

    public int getBattery() {
        return battery;
    }
}
