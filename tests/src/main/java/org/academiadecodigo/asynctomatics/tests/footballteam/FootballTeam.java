package org.academiadecodigo.asynctomatics.tests.footballteam;

public class FootballTeam implements Comparable<FootballTeam>{

    private int gamesWon;

    public FootballTeam(int gamesWon) {

        if (gamesWon < 0) {
            throw new IllegalArgumentException("Games won should be higher than 0.");
        }

        this.gamesWon = gamesWon;
    }

    @Override
    public int compareTo(FootballTeam otherTeam) {

        // codigo inicial
        /*if (gamesWon > otherTeam.getGamesWon()) {
            return 1;
        }

        if (gamesWon < otherTeam.getGamesWon()) {
            return -1;
        }

        return 0;*/

        // refactor depois dos testes
        return this.getGamesWon() - otherTeam.getGamesWon();
    }

    public int getGamesWon() {
        return gamesWon;
    }

}
