package org.academiadecodigo.asynctomatics.tests.supercalculator;

public interface DisplayResult {

    void showResult(int result);

    void showError(String error);

    // the idea behind this method is to show a prompt to the user asking for some input
    int askUserForOperand();
}
