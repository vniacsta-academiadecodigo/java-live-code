package org.academiadecodigo.asynctomatics.tests.footballteam;

import static org.junit.Assert.*;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;


@RunWith(JUnitParamsRunner.class)
public class FootballTeamTest {

    private static final int ANY_NUMBER = 10;
    private static final int MORE_WINS = 10;
    private static final int LESS_WINS = 1;
    private static final int SAME_GAMES_WON = 0;

    @Test
    @Parameters({
            "1",
            "10",
            "100"})
    public void constructorShouldSetGamesWon(int gamesWon) {

        FootballTeam team = new FootballTeam(gamesWon);

        assertEquals(gamesWon, team.getGamesWon());
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "-1",
            "-100",
            "-34"})
    public void constructorShouldThrowIAEForInvalidGamesWon(int invalidGamesWon) {

        new FootballTeam(invalidGamesWon);
    }

    @Test
    public void shouldBePossibleToCompareTeams() {

        FootballTeam team = new FootballTeam(ANY_NUMBER);

        // verificar se os objetos do tipo FootballTeam sao comparaveis
        // considerando que implementamos o Comparable na classe FootballTeam
        assertTrue(team instanceof Comparable);
    }

    @Test
    public void teamWithMoreMatchesWonShouldBeGreater() {

        FootballTeam teamWithMoreWins = new FootballTeam(MORE_WINS);
        FootballTeam teamWithLessWins = new FootballTeam(LESS_WINS);

        assertTrue(teamWithMoreWins.compareTo(teamWithLessWins) > 0);
    }

    @Test
    public void teamWithLessMatchesWonShouldBeGreater() {

        FootballTeam teamWithMoreWins = new FootballTeam(MORE_WINS);
        FootballTeam teamWithLessWins = new FootballTeam(LESS_WINS);

        assertTrue(teamWithLessWins.compareTo(teamWithMoreWins) < 0);
    }

    @Test
    public void teamWithSameMatchesWonShouldBeEqual() {

        FootballTeam teamA = new FootballTeam(SAME_GAMES_WON);
        FootballTeam teamB = new FootballTeam(SAME_GAMES_WON);

        assertEquals(0, teamA.compareTo(teamB));
    }
}
