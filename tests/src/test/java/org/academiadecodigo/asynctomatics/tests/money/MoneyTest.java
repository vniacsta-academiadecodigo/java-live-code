package org.academiadecodigo.asynctomatics.tests.money;

// para usar testes vamos usar a framework JUnit
// para isso temos de colocar a dependencia no pom
// e depois dizer ao metodo dos testes que queremos trabalhar com JUnit -> @Test

import junitparams.Parameters;
import junitparams.converters.Nullable;
import org.junit.Test;

// static import -> importar todos os metodos estaticos que existem na classe
// em vez de ter sempre Assert.assertEquals(), só precisamos de ter assertEquals()
import static org.junit.Assert.*;

// framework que ajuda a definir varios parametros
// https://github.com/Pragmatists/JUnitParams
// não esquecer de adicionar ao pom
// aplicação da framework com @RunWith -> nao é necessário colocar nada nas dependencias
import junitparams.JUnitParamsRunner;
import org.junit.runner.RunWith;


@RunWith(JUnitParamsRunner.class)
public class MoneyTest {

    // os testes normalmente não tem propriedades/estado
    // só criamos estas propriedades para ser mais legivel
    private static final String VALID_CURRENCY = "EUR";
    private static final int VALID_AMOUNT = 10;

    // testes vão ser sempre public e void
    // devem ter sempre um nome descritivo p ser claro o que vamos fazer
    @Test
    @Parameters({
            "10",
            "20",
            "100"})
    public void constructorShouldSetAmount(int amount) {

        // temos de instanciar a classe que vamos testar dentro do metodo, podemos criar como propriedade, caso
        // esteja a ser usada em MUITOS metodos, mas o mais correto é instanciar sempre dentro do metodo que vamos testar
        Money money = new Money(amount, VALID_CURRENCY);

        // aqui os argumentos são o valor expected e o valor do getter - valor que a propriedade tem naquele momento
        assertEquals(amount, money.getValue());
    }

    @Test
    @Parameters({
            "EUR",
            "USD",
            "GBP"})
    public void constructorShouldSetCurrency(String currency) {

        Money money = new Money(VALID_AMOUNT, currency);

        assertEquals(currency, money.getCurrency());
    }

    // testar se a exceção no constructor está a funcionar
    // temos de dizer ao teste que vai receber uma exceção
    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "0",
            "-10",
            "-123455"})
    public void constructorShouldThrowIAEForInvalidAmount(int invalidAmount) {

        new Money(invalidAmount, VALID_CURRENCY);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({
            "",
            "null"})
    // uma string pode ter valor null, mas como a framework exige que os parametros tenham "" e null é atribuido
    // sem "", temos de dizer ao parametro que vai estar à escuta de um nullable
    public void constructorShouldThrowIAEForInvalidCurrency(@Nullable String invalidCurrency) {

        new Money(VALID_AMOUNT, invalidCurrency);
    }
}
