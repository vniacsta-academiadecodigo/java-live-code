package org.academiadecodigo.asynctomatics.tests.supercalculator;

import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class CalculatorTest {

    private static final int NO_CHARGE_BATTERY_VALUE = 0;
    private static final int CORRECT_AMOUNT = 2;
    private static final Integer FIVE = 5;

    @Test
    public void calculatorWithoutBatteryShouldNotWork() {

        //SETUP

        //Every DOC (Depended On Component) should be a MOCK
        // mock is a method of mockito framework
        BrainServer brain = mock(BrainServer.class);
        DisplayResult display = mock(DisplayResult.class);

        //The class we really want to test should be the real deal
        Calculator calculator = new Calculator(brain, display);

        /*
        As we want to test if our calculator does not work without battery
        and since we don't have, or need, a way to exhaust it,
        we need to make it run dry first invoking the add() method!

        !!! NEVER ALTER THE CLASS API TO EASE YOUR TESTS !!!
         */
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();
        calculator.add();

        //EXERCISE
        calculator.add();

        assertEquals(NO_CHARGE_BATTERY_VALUE, calculator.getBattery());

    }

    //Let's try to run some tests to see Mockito's power in action
    @Test
    public void isDisplaysAskUserForOperandInvokedTheCorrectAmountOfTimes() {

        //SETUP
        BrainServer brain = mock(BrainServer.class);
        DisplayResult display = mock(DisplayResult.class);
        Calculator calculator = new Calculator(brain, display);

        //EXERCISE
        //Test if the display.askUserForOperand() is invoked two times
        calculator.add();

        // verify is a method of mockito framework
        verify(display, times(CORRECT_AMOUNT)).askUserForOperand();
    }

    @Test
    public void isBrainAddInvokedTheCorrectAmountOfTimes() {

        //SETUP
        BrainServer brain = mock(BrainServer.class);
        DisplayResult display = mock(DisplayResult.class);
        Calculator calculator = new Calculator(brain, display);

        //Prepare display to act as we want it to
        // when is a much used method in mockito that allows you to pretend that
        // that object is doing something
        // we need this because we are testing only one object in this assertion
        when(display.askUserForOperand()).thenReturn(FIVE);

        //EXERCISE
        calculator.add();

        verify(brain, times(1)).add(FIVE, FIVE);
    }

    @Test
    public void isBrainAddInvokedWithTheCorrectArguments() {

        //SETUP
        BrainServer brain = mock(BrainServer.class);
        DisplayResult display = mock(DisplayResult.class);
        Calculator calculator = new Calculator(brain, display);

        //Prepare display to act as we want it to
        when(display.askUserForOperand()).thenReturn(FIVE);

        //EXERCISE
        calculator.add();

        verify(display).showResult(anyInt());
        verify(brain).add(FIVE, FIVE);

    }
}
